from flask import Flask, request, render_template
from flask_redis import FlaskRedis
from redis.exceptions import RedisError, ConnectionError
import os

kb = 1024
max_key_length = 128

app = Flask(__name__)
app.config['REDIS_URL'] = os.getenv(
    "REDIS_URL",
    "redis://localhost:6379/0").strip()
app.logger.error(app.config['REDIS_URL'])
redis_client = FlaskRedis(app, strict=True, decode_responses=True)


@app.route('/')
def home():
    keys = []
    error = ''
    try:
        cursor, keys = redis_client.scan(cursor=0, count=20)
    except ConnectionError as e:
        app.logger.error(f'Could not connect to database: {e}')
        error = 'Could not connect to database'
    except RedisError as e:
        app.logger.error(f'Could not fetch keys from database: {e}')
        error = 'Could not get keys from the database'
    return render_template('index.html', keys=keys, error=error)


@app.route('/push', methods=['POST', 'GET'])
def push():
    if request.method == 'POST':
        key = request.form['key']
        value = request.form['value']
        if key and value and \
            len(key) < max_key_length and \
                len(value.encode('utf-8')) <= kb:

            try:
                redis_client.set(key, value, ex=30)
                return render_template('push.html', key=key, value=value)
            except RedisError as e:
                app.logger.error(f"redis error: {e}")
                error = f'Could not set {key} in database'
                return render_template('push.html', error=error)
        else:
            error = 'Key or value is empty or too large'
            return render_template('push.html', error=error)
    else:
        return render_template('push.html')


@app.route('/get', methods=['GET'])
def get():
    key = request.args.get('key', '')
    value = None

    if key == '':
        return render_template('get.html')
    try:
        value = redis_client.get(key)
    except RedisError as e:
        app.logger.error(f"redis error: {e}")
        error = f'Could not get {key} from database'
        return render_template('get.html', error=error)

    return render_template('get.html', key=key, value=value)
