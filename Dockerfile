FROM python:3.9-buster

WORKDIR /app

# See https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN useradd -M app

USER app
COPY web/ .
EXPOSE 8000:8000
ENTRYPOINT [ "gunicorn", "-b :8000", "-w 2", "main:app" ]
