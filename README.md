# A simple Flask app with Redis backend

An example application which is part of CI/CD pipeline.

## How to run

Have the following tools installed:
- python3
- python3-pip
- python3-venv
- docker
- docker-compose

Start the webserver with:
```shell
docker-compose up --build
# or detached
docker-compose up --build -d
```
